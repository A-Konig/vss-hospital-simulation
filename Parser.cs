﻿
namespace HospitalSimulation
{
    class Parser
    {
        /// <summary>
        /// Parse integer
        /// </summary>
        /// <param name="str"> Input string </param>
        /// <returns> Integer value </returns>
        internal static int ParseInt(string str)
        {
            int num = int.MinValue;
            bool res = int.TryParse(str, out num);

            return num;
        }

        /// <summary>
        /// Parse multiple ints separated by ; from one string
        /// </summary>
        /// <param name="text"></param>
        /// <returns> Array of ints </returns>
        internal static int[] ParseMultipleInts(string text)
        {
            string[] input = text.Split(';');
            int[] output = new int[input.Length];

            for (int i = 0; i < input.Length; i++)
            {
                output[i] = int.MinValue;
                bool res = int.TryParse(input[i].Trim(), out output[i]);
            }

            return output;
        }

        /// <summary>
        /// Parse double
        /// </summary>
        /// <param name="text"> Input string </param>
        /// <returns> Double value </returns>
        internal static double ParseDouble(string text)
        {
            double num = double.MinValue;
            bool res = double.TryParse(text.Trim(), out num);

            return num;
        }

        /// <summary>
        /// Parse multiple doubles separated by ; from one string
        /// </summary>
        /// <param name="text"> Input string </param>
        /// <returns> Array of doubles </returns>
        internal static double[] ParseMultipleDoubles(string text)
        {
            string[] input = text.Split(';');
            double[] output = new double[input.Length];

            for (int i = 0; i < input.Length; i++)
            {
                output[i] = int.MinValue;
                bool res = double.TryParse(input[i].Trim(), out output[i]);
            }

            return output;
        }
    }
}
