﻿using System.Collections.Generic;

namespace HospitalSimulation
{

	/// <summary>
	/// Class serving as statistics collector
	/// </summary>
	static class Statistics
	{
		// graph 1
		public static List<int> PeopleInQueueHistory { get; set; }
		public static List<int> RegisteredPatientsHistory { get; set; }
		public static List<int> PeopleDiedHistory { get; set; }

		// graph 2
		public static List<int> PeopleDiedInQueueHistory { get; set; }
		public static List<int> PeopleDiedInSTDHistory { get; set; }
		public static List<int> PeopleDiedInICUHistory { get; set; }

		// graph 4
		public static List<double> DeathProbHistory { get; set; }

		// graph 3
		public static List<int> PeopleInICUHistory { get; set; }
		public static List<int> PeopleInSTDHistory { get; set; }

		public static int PeopleInHospital { get; set; }
		public static int NumPatientsDiedWaiting { get; set; } 
		public static int NumPatientsDiedInStd { get; set; }
		public static int NumPatientsDiedInICU { get; set; }
		public static int NumPatientsCured { get; set; }
		public static int PatientsInSTD { get; internal set; }
		public static int PatientsInICU { get; internal set; }

		public static int NumPatientsWaitingForStd { get; set; } // not updated
		public static int NumPatientsWaitingForICU { get; set; } // not updated
		public static double PatientDeathProbability { get; set; } // not updated; todo how to calculate?

		public static int PeopleInQueue { get; set; }
		public static int PeopleDiedInQueue_D { get; set; }
		public static int PeopleDiedInSTD_D { get; set; }
		public static int PeopleDiedInICU_D { get; set; }

		public static int PeopleVisitedHospital { get; set; }


		public static void Reset()
		{
			PeopleInQueueHistory = new List<int>();
			RegisteredPatientsHistory = new List<int>();
			PeopleDiedHistory = new List<int>();
			PeopleDiedInQueueHistory = new List<int>();
			PeopleDiedInSTDHistory = new List<int>();
			PeopleDiedInICUHistory = new List<int>();
			PeopleInICUHistory = new List<int>();
			PeopleInSTDHistory = new List<int>();
			DeathProbHistory = new List<double>();

			PeopleInHospital = 0;
			NumPatientsDiedWaiting = 0;
			NumPatientsDiedInStd = 0;
			NumPatientsDiedInICU = 0;
			NumPatientsCured = 0;
			NumPatientsWaitingForStd = 0;
			NumPatientsWaitingForICU = 0;
			PatientDeathProbability = 0;
			PeopleInQueue = 0;
			PeopleDiedInQueue_D = 0;
			PeopleDiedInSTD_D = 0;
			PeopleDiedInICU_D = 0;
			PeopleVisitedHospital = 0;
		}
		
	}
}
