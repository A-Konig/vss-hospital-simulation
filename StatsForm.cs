﻿using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace HospitalSimulation
{
    /// <summary>
    /// Form for graphs with statistics
    /// </summary>
    public partial class StatsForm : Form
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public StatsForm()
        {
            InitializeComponent();
            SetUpGraph1();
            SetUpGraph2();
            SetUpGraph3();
            SetUpGraph4();
        }

        /// <summary>
        /// Set graph 1 - number of patients in hospital, people in queue and number of deaths in ONE DAY
        /// </summary>
        private void SetUpGraph1()
        {
            Series series1 = new Series("# of people in queue");
            series1.Points.DataBindY(Statistics.PeopleInQueueHistory.ToArray());
            series1.ChartType = SeriesChartType.Column;

            Series series2 = new Series("# of patients in hospital");
            series2.Points.DataBindY(Statistics.RegisteredPatientsHistory.ToArray());
            series2.ChartType = SeriesChartType.Column;

            Series series3 = new Series("# of people died");
            series3.Points.DataBindY(Statistics.PeopleDiedHistory.ToArray());
            series3.ChartType = SeriesChartType.Column;

            // add each series to the chart
            pplInHospCHRT.Series.Clear();
            pplInHospCHRT.Series.Add(series1);
            pplInHospCHRT.Series.Add(series2);
            pplInHospCHRT.Series.Add(series3);

            // additional styling
            pplInHospCHRT.ResetAutoValues();
            pplInHospCHRT.Titles.Clear();
            pplInHospCHRT.Titles.Add("PEOPLE IN HOSPITAL VS DEATHS IN A DAY");
            pplInHospCHRT.ChartAreas[0].AxisX.Title = "Day";
            pplInHospCHRT.ChartAreas[0].AxisY.Title = "# people";
            pplInHospCHRT.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.LightGray;
            pplInHospCHRT.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.LightGray;
        }

        /// <summary>
        /// Set up graph 2 - Death distribution in ONE DAY
        /// </summary>
        private void SetUpGraph2()
        {
            Series series1 = new Series("# of people died in queue");
            series1.Points.DataBindY(Statistics.PeopleDiedInQueueHistory.ToArray());
            series1.ChartType = SeriesChartType.Column;

            Series series2 = new Series("# of people died in STD");
            series2.Points.DataBindY(Statistics.PeopleDiedInSTDHistory.ToArray());
            series2.ChartType = SeriesChartType.Column;

            Series series3 = new Series("# of people died in ICU");
            series3.Points.DataBindY(Statistics.PeopleDiedInICUHistory.ToArray());
            series3.ChartType = SeriesChartType.Column;

            // add each series to the chart
            deathCHRT.Series.Clear();
            deathCHRT.Series.Add(series1);
            deathCHRT.Series.Add(series2);
            deathCHRT.Series.Add(series3);

            // additional styling
            deathCHRT.ResetAutoValues();
            deathCHRT.Titles.Clear();
            deathCHRT.Titles.Add("DEATH DISTRIBUTION IN A DAY");
            deathCHRT.ChartAreas[0].AxisX.Title = "Day";
            deathCHRT.ChartAreas[0].AxisY.Title = "# people";
            deathCHRT.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.LightGray;
            deathCHRT.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.LightGray;
        }

        /// <summary>
        /// Set up graph 3 - Patient distribution in ONE DAY
        /// </summary>
        private void SetUpGraph3()
        {
            Series series1 = new Series("# od people in STD");
            series1.Points.DataBindY(Statistics.PeopleInSTDHistory.ToArray());
            series1.ChartType = SeriesChartType.Column;

            Series series2 = new Series("# of people in ICU");
            series2.Points.DataBindY(Statistics.PeopleInICUHistory.ToArray());
            series2.ChartType = SeriesChartType.Column;

            // add each series to the chart
            patDistCHRT.Series.Clear();
            patDistCHRT.Series.Add(series1);
            patDistCHRT.Series.Add(series2);

            // additional styling
            patDistCHRT.ResetAutoValues();
            patDistCHRT.Titles.Clear();
            patDistCHRT.Titles.Add("PATIENT DISTRIBUTION IN A DAY");
            patDistCHRT.ChartAreas[0].AxisX.Title = "Day";
            patDistCHRT.ChartAreas[0].AxisY.Title = "# people";
            patDistCHRT.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.LightGray;
            patDistCHRT.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.LightGray;
        }

        /// <summary>
        /// Set up graph 4 - Death probability over time
        /// </summary>
        private void SetUpGraph4()
        {
            Series series1 = new Series("Probability of death");
            series1.Points.DataBindY(Statistics.DeathProbHistory.ToArray());
            series1.ChartType = SeriesChartType.Column;
            series1.Color = Color.OrangeRed;

            // add each series to the chart
            deathProbCHRT.Series.Clear();
            deathProbCHRT.Series.Add(series1);

            // additional styling
            deathProbCHRT.ResetAutoValues();
            deathProbCHRT.Titles.Clear();
            deathProbCHRT.Titles.Add("DEATH PROBABILITY OVERALL");
            deathProbCHRT.ChartAreas[0].AxisX.Title = "Day";
            deathProbCHRT.ChartAreas[0].AxisY.Title = "Prob of death in %";
            deathProbCHRT.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.LightGray;
            deathProbCHRT.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.LightGray;
        }
    }
}
