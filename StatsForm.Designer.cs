﻿
namespace HospitalSimulation
{
    partial class StatsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatsForm));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pplInHospCHRT = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.deathCHRT = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.patDistCHRT = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.deathProbCHRT = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pplInHospCHRT)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deathCHRT)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patDistCHRT)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deathProbCHRT)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(801, 448);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pplInHospCHRT);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(793, 422);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Graph 1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pplInHospCHRT
            // 
            chartArea1.Name = "ChartArea1";
            this.pplInHospCHRT.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.pplInHospCHRT.Legends.Add(legend1);
            this.pplInHospCHRT.Location = new System.Drawing.Point(8, 6);
            this.pplInHospCHRT.Name = "pplInHospCHRT";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.pplInHospCHRT.Series.Add(series1);
            this.pplInHospCHRT.Size = new System.Drawing.Size(779, 416);
            this.pplInHospCHRT.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.deathCHRT);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(793, 422);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Graph 2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // deathCHRT
            // 
            chartArea2.Name = "ChartArea1";
            this.deathCHRT.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.deathCHRT.Legends.Add(legend2);
            this.deathCHRT.Location = new System.Drawing.Point(6, 6);
            this.deathCHRT.Name = "deathCHRT";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.deathCHRT.Series.Add(series2);
            this.deathCHRT.Size = new System.Drawing.Size(779, 410);
            this.deathCHRT.TabIndex = 1;
            this.deathCHRT.Text = "chart2";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.patDistCHRT);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(793, 422);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Graph 3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // patDistCHRT
            // 
            chartArea3.Name = "ChartArea1";
            this.patDistCHRT.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.patDistCHRT.Legends.Add(legend3);
            this.patDistCHRT.Location = new System.Drawing.Point(6, 6);
            this.patDistCHRT.Name = "patDistCHRT";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.patDistCHRT.Series.Add(series3);
            this.patDistCHRT.Size = new System.Drawing.Size(779, 410);
            this.patDistCHRT.TabIndex = 1;
            this.patDistCHRT.Text = "chart3";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.deathProbCHRT);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(793, 422);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Graph 4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // deathProbCHRT
            // 
            chartArea4.Name = "ChartArea1";
            this.deathProbCHRT.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.deathProbCHRT.Legends.Add(legend4);
            this.deathProbCHRT.Location = new System.Drawing.Point(7, 6);
            this.deathProbCHRT.Name = "deathProbCHRT";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.deathProbCHRT.Series.Add(series4);
            this.deathProbCHRT.Size = new System.Drawing.Size(779, 410);
            this.deathProbCHRT.TabIndex = 2;
            this.deathProbCHRT.Text = "chart3";
            // 
            // StatsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StatsForm";
            this.Text = "Statistics";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pplInHospCHRT)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deathCHRT)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.patDistCHRT)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deathProbCHRT)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataVisualization.Charting.Chart pplInHospCHRT;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataVisualization.Charting.Chart deathCHRT;
        private System.Windows.Forms.DataVisualization.Charting.Chart patDistCHRT;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataVisualization.Charting.Chart deathProbCHRT;
    }
}