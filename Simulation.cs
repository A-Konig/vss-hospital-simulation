﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace HospitalSimulation
{
    /// <summary>
    /// Class responsible for running the simulation
    /// </summary>
    class Simulation
    {
        /// <summary> Simulation window </summary>
        SimulationWindow window;

        /// <summary> Is simulation running </summary>
        public bool runSimulation { get; set; }
        /// <summary> Is simulation paused </summary>
        bool pausedSimulation;

        /// <summary> List of todays patients </summary>
        List<Patient> patientsToday;
        /// <summary> List of tommorrows patients </summary>
        List<Patient> patientsTommorrow;

        /// <summary> Actions </summary>
        public List<String> actions;

        /// <summary> Randoms </summary>
        Randoms rands;

        // Default values for input
        public int DefaultStdCapacity { get; } = 500; 
        public int DefaultJIPCapacity { get; } = 100;
        public double DefaultPatArrMean { get; } = 5;
        public double DefaultHospStayMean { get; } = 4;
        public double DefaultHospStayStDev { get; } = 2;
        public double DefaultJIPStayMean { get; } = 7;
        public double DefaultJIPMoveP { get; } = 0.4;
        public double DefaultStdDeathP { get; } = 0.03; 
        public double DefaultJIPDeathP { get; } = 0.02;
        public double DefaultTransferDeathP { get; } = 0.01;
        public double DefaultQueueStayMean { get; } = 4;
        public double DefaultQueueStayStDev { get; } = 1;


        // User input values
        public int StdCapacity { get; set; }  
        public int ICUCapacity { get; set; }

        public double PatArrMean { get; set; }
        
        public double HospStayMean { get; set; }
        public double HospStayStDev { get; set; }

        public double QueueStayMean { get; set; }
        public double QueueStayStDev { get; set; }

        public double ICUStayMean { get; set; } 
        
        public double ICUMoveP { get; set; } 
        public double StdDeathP { get; set; } 
        public double ICUDeathP { get; set; } 
        public double transferDeathP { get; set; }
        
        /// <summary> Simulation day </summary>
        int simulationDay = 1;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="window"></param>
        public Simulation(SimulationWindow window)
        {
            this.window = window;

            runSimulation = false;
            patientsToday = new List<Patient>();
            patientsTommorrow = new List<Patient>();
            actions = new List<string>();

            rands = new Randoms();
        }

        /// <summary>
        /// Simulation loop
        /// </summary>
        public void SimRoutine()
        {
            runSimulation = true;
            while (runSimulation)
            {
                // if simulation paused -> wait
                while (pausedSimulation)
                    Thread.Sleep(1000);

                actions.Add("DAY " + simulationDay + " ------------------------------- ");

                // generate new patients
                int newPatients = rands.NextPoisson(PatArrMean);
                Statistics.PeopleInHospital += newPatients;
                for (int i = 0; i < newPatients; i++)
                {
                    string name = rands.NextName();
                    int daysInHospital = Math.Max(1, (int) rands.NextGaussian(HospStayMean, HospStayStDev));
                    int maxInQueue = Math.Max(1, (int)rands.NextGaussian(QueueStayMean, QueueStayStDev));

                    Patient p = new Patient() { Name = name, Position = PatientPosition.WAITING_FOR_STD, DaysToSpendInSTD = daysInHospital,  QueueWaitUntilDeath = maxInQueue };
                    actions.Add($"{name} arrived to hospital.");
                    patientsToday.Add(p);
                    Statistics.PeopleInQueue++;
                    Statistics.PeopleVisitedHospital++;
                }

                // update statistics - in the morning
                Statistics.RegisteredPatientsHistory.Add(Statistics.PatientsInICU + Statistics.PatientsInSTD);
                Statistics.PeopleInQueueHistory.Add(Statistics.PeopleInQueue);
                double deathProb = 0;
                int deathsAll = Statistics.NumPatientsDiedInICU + Statistics.NumPatientsDiedInStd + Statistics.NumPatientsDiedWaiting;
                if (deathsAll != 0)
                    deathProb = ((double)(deathsAll) / Statistics.PeopleVisitedHospital) * 100;
                Statistics.DeathProbHistory.Add(deathProb);
                Statistics.PatientDeathProbability = deathProb;

                // reset day statistics
                Statistics.PeopleDiedInICU_D = 0;
                Statistics.PeopleDiedInQueue_D = 0;
                Statistics.PeopleDiedInSTD_D = 0;

                // run operations on all patients
                for (int i = 0; i < patientsToday.Count; i++)
                {
                    Patient currP = patientsToday[i];
                    switch (currP.Position)
                    {
                        // patient waiting for standard care
                        case PatientPosition.WAITING_FOR_STD:
                            {
                                PatientInQueue(currP);
                                break;
                            }

                        // patient waiting for standard care
                        case PatientPosition.WAITING_FOR_STD_IN_ICU:
                            {
                                PatientInJIPToSTD(currP);
                                break;
                            }

                        // patient in standard care
                        case PatientPosition.STAYING_IN_STD:
                            {
                                PatientInSTD(currP);
                                break;
                            }

                        // patient waiting for intensive care - this case now redundant
                        case PatientPosition.WAITING_FOR_ICU:
                            {
                                PatientInSTDtoICU(currP);
                                break;
                            }

                        // patient in intensive care
                        case PatientPosition.STAYING_IN_ICU:
                            {
                                PatientInICU(currP);
                                break;
                            }
                    }
                }

                // update statistics - evening
                Statistics.PeopleInICUHistory.Add(Statistics.PatientsInICU);
                Statistics.PeopleInSTDHistory.Add(Statistics.PatientsInSTD);

                int pplDied_D = Statistics.PeopleDiedInICU_D + Statistics.PeopleDiedInSTD_D + Statistics.PeopleDiedInQueue_D;
                Statistics.PeopleDiedInICUHistory.Add(Statistics.PeopleDiedInICU_D);
                Statistics.PeopleDiedInSTDHistory.Add(Statistics.PeopleDiedInSTD_D);
                Statistics.PeopleDiedInQueueHistory.Add(Statistics.PeopleDiedInQueue_D);
                Statistics.PeopleDiedHistory.Add(pplDied_D);

                // display current situation
                simulationDay++;
                window.DisplayCurrentSituation();
                window.DisplayCurrentStats();

                Thread.Sleep(2000);

                // swap patient lists
                patientsToday = patientsTommorrow;
                patientsTommorrow = new List<Patient>();
            }

        }

        /// <summary>
        /// Patient in ICU
        /// </summary>
        /// <param name="currP"></param>
        private void PatientInICU(Patient currP)
        {
			// if can be moved move directly, if not go to waiting for std
			if (currP.DaysSpentInICU >= currP.DaysToSpendInICU)
			{
                // wants to move back to std
                PatientInJIPToSTD(currP); // is this legal?????
                return; 

            }
			else
                currP.DaysSpentInICU++;

            // can die here
            double r = rands.NextDouble();
            if (r <= ICUDeathP)
            {
                actions.Add("Patient " + currP.Name + " died in ICU :(");
                Statistics.NumPatientsDiedInICU++;
                Statistics.PeopleInHospital--;
                Statistics.PatientsInICU--;
                Statistics.PeopleDiedInICU_D++;
                return;
            }

            patientsTommorrow.Add(currP);
        }

        /// <summary>
        /// Patient waits in STD to be admitted to ICU
        /// </summary>
        /// <param name="currP"></param>
        private void PatientInSTDtoICU(Patient currP)
        {
            // can die here
            double r = rands.NextDouble();
            if (r <= StdDeathP)
            {
                actions.Add("Patient " + currP.Name + " died waiting for ICU :(");
                Statistics.NumPatientsDiedWaiting++;
                Statistics.PeopleInHospital--;
                Statistics.PatientsInSTD--;
                Statistics.PeopleDiedInSTD_D++;
                return;
            }

            // can be transferred
            if (Statistics.PatientsInICU < ICUCapacity)
            {
                currP.Position = PatientPosition.STAYING_IN_ICU;
                int daysInJIP = (int)rands.NextExp(ICUStayMean);
                currP.DaysToSpendInICU = daysInJIP;
                Statistics.PatientsInSTD--;
                Statistics.PatientsInICU++;
                actions.Add("Patient " + currP.Name + " moved to ICU, should stay for " + currP.DaysToSpendInICU + " days.");
            }

            patientsTommorrow.Add(currP);
        }

        /// <summary>
        /// Patient in STD
        /// </summary>
        /// <param name="currP"></param>
        private void PatientInSTD(Patient currP)
        {

            // could be released here - check before giving them a chance to die:
            if(currP.DaysSpentInSTD >= currP.DaysToSpendInSTD)
			{
                actions.Add("Patient " + currP.Name + " went home cured. :)");
                Statistics.NumPatientsCured++;
                Statistics.PeopleInHospital--;
                Statistics.PatientsInSTD--;
                return;
            }

            // can die here
            double r = rands.NextDouble();
            if (r <= ICUDeathP)
            {
                actions.Add("Patient " + currP.Name + " died in standard care :(");
                Statistics.NumPatientsDiedInStd++;
                Statistics.PeopleInHospital--;
                Statistics.PatientsInSTD--;
                Statistics.PeopleDiedInSTD_D++;
                return;
            }

            // roll random for move to icu
            r = rands.NextDouble();
            if (r <= ICUMoveP)
            {
                int daysInICU = (int)rands.NextExp(ICUStayMean);
                currP.DaysToSpendInICU = daysInICU;

                // can be transferred
                if (Statistics.PatientsInICU < ICUCapacity)
                {
                    currP.Position = PatientPosition.STAYING_IN_ICU;
                    Statistics.PatientsInSTD--;
                    Statistics.PatientsInICU++;
                    actions.Add("Patient " + currP.Name + " moved to ICU, should stay for " + currP.DaysToSpendInICU + " days.");
                }
                // else waits for space
                else
                {
                    actions.Add("Patient " + currP.Name + " died waiting for ICU :(");
                    Statistics.NumPatientsDiedInStd++;
                    Statistics.PeopleInHospital--;
                    Statistics.PatientsInSTD--;
                    Statistics.PeopleDiedInSTD_D++;
                    return;

                    /*
                    currP.Position = PatientPosition.WAITING_FOR_ICU;
                    actions.Add("Patient " + currP.Name + " waits for ICU");
                    currP.DaysSpentInSTD++;
                    */
                }
            }
			else
			{
                currP.DaysSpentInSTD++;
            }

            patientsTommorrow.Add(currP);
        }

        /// <summary>
        /// Patient in JIP waits to be admitted to STD
        /// </summary>
        /// <param name="currP"></param>
        private void PatientInJIPToSTD(Patient currP)
        {
            // can die here
            double r = rands.NextDouble();
            if (r <= transferDeathP)
            {
                actions.Add("Patient " + currP.Name + " died in ICU :(");
                Statistics.NumPatientsDiedInICU++;
                Statistics.PeopleInHospital--;
                Statistics.PatientsInICU--;
                Statistics.PeopleDiedInICU_D++;
                return;
            }

            // can be admitted
            if (Statistics.PatientsInSTD < StdCapacity)
            {
                currP.Position = PatientPosition.STAYING_IN_STD;
                currP.DaysToSpendInSTD = Math.Max(1, (int)rands.NextGaussian(HospStayMean, HospStayStDev)); // assign new days to spend in std
                actions.Add("Patient " + currP.Name + " moved back to standard care, needs " + currP.DaysToSpendInSTD + " days of STD care.");
                Statistics.PatientsInSTD++;
                Statistics.PatientsInICU--;
            }

            patientsTommorrow.Add(currP);
        }

        /// <summary>
        /// New person wants to be admitted
        /// </summary>
        /// <param name="currP"></param>
        private void PatientInQueue(Patient currP)
        {
			if (currP.DaysSpentInSTDQueue >= currP.QueueWaitUntilDeath)
			{
				actions.Add("Patient " + currP.Name + " died in queue :(");
                Statistics.NumPatientsDiedWaiting++;
                Statistics.PeopleInHospital--;
                Statistics.PeopleInQueue--;
                Statistics.PeopleDiedInQueue_D++;
                return;
			}

            // can be admitted
            if (Statistics.PatientsInSTD < StdCapacity)
            {
                currP.Position = PatientPosition.STAYING_IN_STD;
                Statistics.PatientsInSTD++;
                Statistics.PeopleInQueue--;
                actions.Add("Patient " + currP.Name + " admitted. They need " + currP.DaysToSpendInSTD + " days in STD care.");
            }
			else
			{
                // cannot be admitted, has to keep waiting
                //  => do not change position, only increase qWait counter:
                currP.DaysSpentInSTDQueue++;
                actions.Add("Patient " + currP.Name + " has been waiting for admition for " + currP.DaysSpentInSTDQueue + " days.");
            }

            patientsTommorrow.Add(currP);
        }

        /// <summary>
        /// Pause simulation
        /// </summary>
        internal void Pause()
        {
            pausedSimulation = !pausedSimulation;
        }

        /// <summary>
        /// Stop simulation
        /// </summary>
        internal void Stop()
        {
            runSimulation = false;
            pausedSimulation = false;
            patientsToday = new List<Patient>();
            patientsTommorrow = new List<Patient>();
            actions = new List<string>();
            simulationDay = 1;
            Statistics.PeopleInQueue = 0;
            Statistics.PatientsInICU = 0;
            Statistics.PatientsInSTD = 0;

            Statistics.Reset();
            rands.Reset();
        }

    }
}
