﻿
namespace HospitalSimulation
{

    /// <summary>
    /// Enum with possible positions in hospital
    /// </summary>
    enum PatientPosition
	{
        WAITING_FOR_STD,
        WAITING_FOR_ICU,
        WAITING_FOR_STD_IN_ICU,
        STAYING_IN_STD,
        STAYING_IN_ICU
	}

    /// <summary>
    /// Class representing one patient
    /// </summary>
    class Patient
    {

        /// <summary> Name of patient </summary>
        public string Name { get; set; }
        /// <summary> Position in hospital </summary>
        public PatientPosition Position { get; set; }

        /// <summary> Days the patient should spend in STD </summary>
        public int DaysToSpendInSTD { get; set; }
        /// <summary> Days the patient should spend in ICU </summary>
        public int DaysToSpendInICU { get; set; }
        /// <summary> Days the patient spent in STD </summary>
        public int DaysSpentInSTD { get; set; }
        /// <summary> Days the patient spent in ICU </summary>
        public int DaysSpentInICU { get; set; }
     
        /// <summary> Days the patient can spend in queue </summary>
        public int QueueWaitUntilDeath { get; set; }
        /// <summary> Days the patient spent in queue </summary>
        public int DaysSpentInSTDQueue { get; set; }
    }
}
