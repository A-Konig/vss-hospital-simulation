﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HospitalSimulation
{
    /// <summary>
    /// Class with random generators
    /// </summary>
    class Randoms
    {
        /// <summary> Random generator used for Gaussian distribution </summary>
        private Random randG;
        /// <summary> Random generator used for Poisson distribution </summary>
        private Random randP; 
        /// <summary> Basic random </summary>
        private Random rand;

        /// <summary> List with availible names </summary>
        private List<string> names;
        /// <summary> Index of next name </summary>
        private int nextName;

        public static string pathToF = "./data/";

        /// <summary>
        /// Constructor
        /// </summary>
        public Randoms()
		{
            randG = new Random();
            randP = new Random();
            rand = new Random();

            LoadNames();
        }

        /// <summary>
        /// Load names from file
        /// Names generated using https://fossbytes.com/tools/random-name-generator
        /// </summary>
        private void LoadNames()
		{
            string path =  pathToF + "random_names_fossbytes.txt";
            string[] lines = File.ReadAllLines(path);
            names = new List<string>(lines);
        }

        /// <summary>
        /// Generate next gaussian distributed number
        /// Using Box-Müller transform: https://stackoverflow.com/questions/218060/random-gaussian-variables
        /// </summary>
        /// <param name="mean"> Mean </param>
        /// <param name="stdDev"> Standard deviation </param>
        /// <returns> Double number </returns>
        public double NextGaussian(double mean, double stdDev)
        {
            double u1 = 1.0 - randG.NextDouble(); //uniform(0,1] random doubles
            double u2 = 1.0 - randG.NextDouble();
            double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2); //random normal(0,1)
            double randNormal = mean + stdDev * randStdNormal; //random normal(mean,stdDev^2)
            return randNormal;
        }

        /// <summary>
        /// Generate next exponentially distributed number
        /// https://stackoverflow.com/questions/29020652/java-exponential-distribution
        /// </summary>
        /// <param name="mean"> Mean </param>
        /// <returns> Double number </returns>
        public double NextExp(double mean)
        {
            double lambda = 1 / mean; // wiki says the mean of exp. distribution is 1/lambda
            return Math.Log(1 - rand.NextDouble()) / (-lambda);
        }

        /// <summary>
        /// Generate next poisson distributed number
        /// Using Poisson distribution capable of handling large lambdas
        /// https://en.wikipedia.org/wiki/Poisson_distribution#:~:text=GSL)%3A%20function%20gsl_ran_poisson-,Generating%20Poisson%2Ddistributed%20random%20variables,-A%20simple%20algorithm 
        /// </summary>
        /// <param name="lambda"> Lambda </param>
        /// <returns> Integer number </returns>
        public int NextPoisson(double lambda)
		{
			double lambdaLeft = lambda;
			int k = 0;
			double p = 1;
			int step = 500;

			do
			{
				k = k + 1;
				double u = randP.NextDouble();
				p = p * u;

				while (p < 1 && lambdaLeft > 0)
				{
					if (lambdaLeft > step)
					{
						p = p * Math.Exp(step);
						lambdaLeft = lambdaLeft - step;
					}
					else
					{
						p = p * Math.Exp(lambdaLeft);
						lambdaLeft = 0;
					}
				}
			}
			while (p > 1);

            return k - 1;
		}


        /// <summary>
        /// Get next name to use
        /// </summary>
        /// <returns> Name </returns>
        public string NextName()
		{
            string name = names[nextName];
            nextName = (nextName + 1) % names.Count;

            return name;
		}

        /// <summary>
        /// Get next equally distributed double
        /// </summary>
        /// <returns> Double number </returns>
        internal double NextDouble()
        {
            return rand.NextDouble();
        }

        /// <summary>
        /// Reset randoms
        /// </summary>
        internal void Reset()
        {
            randG = new Random();
            randP = new Random();
            rand = new Random();
        }
    }
}
