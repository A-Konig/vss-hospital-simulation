# Diskrétní událostní simulace nemocnice
Autoři: Alex König, Eliška Mourycová

## Zadání

Připravit model [sítě front](https://phix.zcu.cz/moodle/mod/quiz/view.php?id=234547 "Sítě front") popisující nemocnici, pokusit se popsat jeho chování a následně implementovat simulaci.

Model obsahuje:
-   Kapacitu nemocnice (počet lůžek základní péče a počet lůžek intenzivní péče)
-   Střední počet pacientů který se každý den do nemocnice dostaví (počty mají poissonovo rozdělení)
-   Střední dobu a směrodatnou odchylku setrvání v nemocnici v základní péči, po ní pacient nemocnici opouští jako zdravý (doba má gaussovo rozdělení).  Pokud se na lůžko pacient nevejde, přidá se k počtu pacientů v dalším dnu.
-   Střední dobu setrvání na lůžku intenzivní péče, po ní se pacient vrací na oddělení základní péče (pokud je volné) (doba má exponenciální rozdělení) Pokud se na lůžko pacient nevejde, umírá.
-   Pravděpodobnost (konstantní) že pacient bude přesunut na intenzivní péči
-   Pravděpodobnost (konstantní) že pacient v základní péči zemře
-   Pravděpodobnost (konstantní) že pacient v intenzivní péči zemře
-   Střední dobu a směrodatnou odchylku, jak dlouho může člověk čekat v čekárně než zemře (dáno gaussovo rozložením)

Pacient je po přijetí do nemocnice umístěn na lůžko základní péče. Zde setrvá po náhodnou dobu určenou daným rozdělením, každý den může být přesunut na jednotku intenzivní péče nebo zemřít. Pokud se na lůžko základní péče nevejde, přidává se do fronty pacientů v dalším dni (zvýší počet příchozích o jedna; pacient čekající na přijetí déle než $d_{wait}$ dní automaticky umírá). Pokud neopustí oddělení, po $d_{std}$ dnech odchází vyléčen.

Pokud je přesunut na jednotku intenzivní péče, setrvá zde opět náhodnou dobu. V každém dni může zemřít. Po ukončení péče (po $d_{icu}$ dnech) se vrací zpět na oddělení základní péče (a dále se s ním nakládá stejně jako by byl nově přijatý). Pokud v oddělení základní péče není místo, zůstává na oddělení intezivní péče dokud se místo neuvolní.

## Model
Model nemocnice je naznačen na následujícím diagramu:

![[diagram.png]]

"Fronty" pro čekání na přijetí a na přesun z JIP do standartní péče jsou nekonečně dlouhé, tedy nové požadavky nejsou odmítány. Požadavek zmizí, pokud pacient ve frontě zemře. 

> mame tu neco rozepsat, nebo to je vlastne podle zadani a nic se nam moc neodchyluje? 

Priorita (pořadí) vyhodnocování operací v každém stavu je následující:
- **arrived**
	1. Pacient je přesunut na standardní lůžko, pokud je nějaké volné
	2. Jinak čeká v čekárně
- **waiting for admission**
	1. Test, jestli se uvolnilo lůžko
	2. Test, jestli pacient čeká *n* dní, pokud ano, pacient umírá
- **STD care**
	1. Test, jestli už může být propuštěn
	2. Test, jestli má umřít
	3. Test, jestli má být přesunut na JIP
- **waiting for ICU**
	1. Automaticky umírá
- **ICU**
	1. Test, jestli se má vrátit na standarní lůžko
	2. Test, jestli má umřít
- **waiting for STD in ICU**
	1. Test, jestli má umřít
	2. Test, jestli se uvolnilo standartní lůžko 


 > mby by se sem dalo napsat jak po sobě nastávaj jednotlivý... věci (je dřív přesun na JIP vs smrt na STD atd) - asi done?

## Stručná živatelská dokumentace
### Spuštění aplikace
Spustitelný soubor se nachází v HospitalSimulation.exe, dávkový skript pro spuštění se nachází v run.bat.

> TODO zabalit do zipu + cesty?

### Ovládání GUI

Ikonka aplikace vytvořena uživatelem juicy_fish na [flaticon.com](https://www.flaticon.com/premium-icon/hospital-building_2749787?term=hospital&related_id=2749787)


![[Pasted image 20211211163741.png]]

Po levé straně hlavního okna aplikace se nacházejí textová pole, do kterých lze zadávat vstupní paramtery simulace popsané v kapitole Zadání.

Pod nimi se nacházejí tlačítka Run, Stop a Pause/Resume. Jejich funkce je následující:
- Run - spouští simulaci
- Stop - ukončí simulaci
- Pause/Resume - pozastaví/znovu spustí simulaci

Během simulace se do prostředního textového pole vypisují události, které proběhly během simulovaných dnů. A na pravé straně se vypisují aktuální statistiky.

Konrkétně se jedná o:
- počet lidí kteří v nemocnici aktuálně nacházejí - započítává se počet přijatých pacientů dohromady s počtem lidí v čekárně
- počet pacientů kteří se v nemocnici aktuálně nacházejí - dále rozepsáno na počet pacientů ve standardní péči (STD) a počet na jednotce intenzivní péče (ICU)
- počet lidí, kteří v nemocnici zemřeli za celý její běh - dále rozepsáno na ty, kteří zemřeli na oddělení STD, ICU a v čekárně
- počet uzdravených pacientů za celý běh nemocnice
- pravděpodobnost smrti pacienta - vypočtena jako poměr celkového počtu lidí, kteří nemocnici navštívili a počtu lidí, kteří v nemocnici zemřeli za celý její běh
 $$ p_{death} = \frac{death\_count}{count} $$

Pod tímto textovým polem je tlačítko Show stats, které otevře okno s grafy, znázorňující průběh některých statistik v čase. Grafy lze zobrazit jen pokud simulace běží nebo pokud je pozastavená tlačítkem Pause.

### Graf 1
Na tomto grafu je znázorněno kolik se na začátku každého dne nacházelo lidí v čekárně, pacientů v nemocnici a kolik z těchto lidí na konci každého dne zemřelo.

![[Pasted image 20211211163755.png]]

### Graf 2
Na tomto grafu je znázorněno kolik lidí každý den zemřelo v čekárně, na STD a na ICU.

![[Pasted image 20211211163803.png]]

### Graf 3
Tento graf obsahuje data o tom, kolik pacientů se nacházelo na konci daného dne na STD a kolik na ICU.

![[Pasted image 20211211163810.png]]

### Graf 4
Tento graf znázorňuje změnu pravděpodobnosti smrti pacienta. Tato pravděpodobnost se počítá na začátku každého dne.

![[Pasted image 20211211163820.png]]


## Rozdělení práce
Viz [https://gitlab.com/A-Konig/vss-hospital-simulation](https://gitlab.com/A-Konig/vss-hospital-simulation).

> Jestli se rozhodnete pracovat na zadání ve skupině, uveďte v dokumentaci všechny členy a podíl jejich práce (je možné si ji libovolně rozdělit) - dal bych odkaz na gitlab asi? nebo to nějak rozepíšem, je mi to jedno :D

## Running TODO
- [x] nakreslit model sítě front
- [x] popis grafů do doc (Alex)
- [ ] dopsat ==death in transfer== do zadání? - momentálně může umřít když se přesouvá z JIP do STD s ppstí death transfer p, neměl by umírat s danou ppstí i při přesunu STD -> JIP? nebo tohle zrušit úplně
- [ ] pacient když se nedočká JIP tak umře podle zadání
- [ ] grafy chování nemocnice
	- [x] zobrazit počet lidí v nemocnici a kolik umřelo za den
	- [x] kolik umřelo v STD kolik v ICU kolik ve frontě
	- [x] zobrazit počet lidí v ICU a STD
	- [ ] chceme někam uzdravené? a kam?
- [ ] komentáře do kódu
- [x] rozdělení - ověřit podle histogramu (Alex - myslim že budu na to mít appku z úkolu 1)
	- [x] gauss
		- 3 dny +- 2 dny
	- [x] poisson
	- [x] exp
		- větší než 1
- [x] vstup pro dobu v čekárně
- [x] doba v čekárně -> gauss
- [x] vlákno místo async pro simulaci
- [x] death probability
- [x] udělat bat na spuštění