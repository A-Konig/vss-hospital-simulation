﻿
namespace HospitalSimulation
{
    partial class SimulationWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SimulationWindow));
            this.hospCapTXT = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.patArrTXT = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.hospStayTXT = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.jipStayTXT = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.stopBT = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.jipMoveTXT = new System.Windows.Forms.TextBox();
            this.stdDeathTXT = new System.Windows.Forms.TextBox();
            this.jipDeathTXT = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.showStatsBT = new System.Windows.Forms.Button();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.pauseBT = new System.Windows.Forms.Button();
            this.deathTransferTXT = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.queueStayTXT = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // hospCapTXT
            // 
            this.hospCapTXT.Location = new System.Drawing.Point(136, 31);
            this.hospCapTXT.Name = "hospCapTXT";
            this.hospCapTXT.Size = new System.Drawing.Size(100, 20);
            this.hospCapTXT.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Hospital capacity";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Mean of patient arrival";
            // 
            // patArrTXT
            // 
            this.patArrTXT.Location = new System.Drawing.Point(136, 63);
            this.patArrTXT.Name = "patArrTXT";
            this.patArrTXT.Size = new System.Drawing.Size(100, 20);
            this.patArrTXT.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Mean + stDev of stay";
            // 
            // hospStayTXT
            // 
            this.hospStayTXT.Location = new System.Drawing.Point(136, 126);
            this.hospStayTXT.Name = "hospStayTXT";
            this.hospStayTXT.Size = new System.Drawing.Size(100, 20);
            this.hospStayTXT.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Mean of ICU stay";
            // 
            // jipStayTXT
            // 
            this.jipStayTXT.Location = new System.Drawing.Point(136, 158);
            this.jipStayTXT.Name = "jipStayTXT";
            this.jipStayTXT.Size = new System.Drawing.Size(100, 20);
            this.jipStayTXT.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(148, 357);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Run";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.runBT_Click);
            // 
            // stopBT
            // 
            this.stopBT.Location = new System.Drawing.Point(12, 357);
            this.stopBT.Name = "stopBT";
            this.stopBT.Size = new System.Drawing.Size(61, 23);
            this.stopBT.TabIndex = 9;
            this.stopBT.Text = "Stop";
            this.stopBT.UseVisualStyleBackColor = true;
            this.stopBT.Click += new System.EventHandler(this.stopBT_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.Location = new System.Drawing.Point(242, 12);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(380, 368);
            this.listBox1.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Move to ICU p.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 256);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Death in stdCU p.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 289);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Death in ICU p.";
            // 
            // jipMoveTXT
            // 
            this.jipMoveTXT.Location = new System.Drawing.Point(136, 189);
            this.jipMoveTXT.Name = "jipMoveTXT";
            this.jipMoveTXT.Size = new System.Drawing.Size(100, 20);
            this.jipMoveTXT.TabIndex = 14;
            // 
            // stdDeathTXT
            // 
            this.stdDeathTXT.Location = new System.Drawing.Point(136, 253);
            this.stdDeathTXT.Name = "stdDeathTXT";
            this.stdDeathTXT.Size = new System.Drawing.Size(100, 20);
            this.stdDeathTXT.TabIndex = 15;
            // 
            // jipDeathTXT
            // 
            this.jipDeathTXT.Location = new System.Drawing.Point(136, 286);
            this.jipDeathTXT.Name = "jipDeathTXT";
            this.jipDeathTXT.Size = new System.Drawing.Size(100, 20);
            this.jipDeathTXT.TabIndex = 16;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.showStatsBT);
            this.groupBox1.Controls.Add(this.listBox2);
            this.groupBox1.Location = new System.Drawing.Point(633, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(223, 368);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Statistics";
            // 
            // showStatsBT
            // 
            this.showStatsBT.Location = new System.Drawing.Point(71, 234);
            this.showStatsBT.Name = "showStatsBT";
            this.showStatsBT.Size = new System.Drawing.Size(75, 23);
            this.showStatsBT.TabIndex = 1;
            this.showStatsBT.Text = "Show stats";
            this.showStatsBT.UseVisualStyleBackColor = true;
            this.showStatsBT.Click += new System.EventHandler(this.showStatsBT_Click);
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.HorizontalScrollbar = true;
            this.listBox2.Location = new System.Drawing.Point(6, 22);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(211, 199);
            this.listBox2.TabIndex = 0;
            // 
            // pauseBT
            // 
            this.pauseBT.Location = new System.Drawing.Point(79, 357);
            this.pauseBT.Name = "pauseBT";
            this.pauseBT.Size = new System.Drawing.Size(63, 23);
            this.pauseBT.TabIndex = 18;
            this.pauseBT.Text = "Pause";
            this.pauseBT.UseVisualStyleBackColor = true;
            this.pauseBT.Click += new System.EventHandler(this.pauseBT_Click);
            // 
            // deathTransferTXT
            // 
            this.deathTransferTXT.Location = new System.Drawing.Point(136, 221);
            this.deathTransferTXT.Name = "deathTransferTXT";
            this.deathTransferTXT.Size = new System.Drawing.Size(100, 20);
            this.deathTransferTXT.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 224);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(97, 13);
            this.label15.TabIndex = 19;
            this.label15.Text = "Death in transfer p.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Mean + stDev of queue";
            // 
            // queueStayTXT
            // 
            this.queueStayTXT.Location = new System.Drawing.Point(136, 95);
            this.queueStayTXT.Name = "queueStayTXT";
            this.queueStayTXT.Size = new System.Drawing.Size(100, 20);
            this.queueStayTXT.TabIndex = 21;
            // 
            // SimulationWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 407);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.queueStayTXT);
            this.Controls.Add(this.deathTransferTXT);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.pauseBT);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.jipDeathTXT);
            this.Controls.Add(this.stdDeathTXT);
            this.Controls.Add(this.jipMoveTXT);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.stopBT);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.jipStayTXT);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.hospStayTXT);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.patArrTXT);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.hospCapTXT);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SimulationWindow";
            this.Text = "Hospital simulation";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox hospCapTXT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox patArrTXT;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox hospStayTXT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox jipStayTXT;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox jipMoveTXT;
		private System.Windows.Forms.TextBox stdDeathTXT;
		private System.Windows.Forms.TextBox jipDeathTXT;
		private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button stopBT;
        private System.Windows.Forms.Button pauseBT;
        private System.Windows.Forms.TextBox deathTransferTXT;
        private System.Windows.Forms.Label label15;
		private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox queueStayTXT;
        private System.Windows.Forms.Button showStatsBT;
    }
}

