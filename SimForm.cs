﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace HospitalSimulation
{
    public partial class SimulationWindow : Form
    {
        /// <summary> Simulation </summary>
        Simulation sim;
        /// <summary> Is simulation paused </summary>
        bool paused;
        /// <summary> Is simulation running </summary>
        bool simRunning;
        /// <summary> Thread with running simulation </summary>
        Thread simThread;

        /// <summary>
        /// Constructor
        /// </summary>
        public SimulationWindow()
        {
            sim = new Simulation(this);
            paused = true;

            FormClosing += Form1_FormClosing;

            InitializeComponent();

            listBox1.DataSource = sim.actions;
            SetDefaults();
            Statistics.Reset();
        }

        /// <summary>
        /// Set default input values
        /// </summary>
        private void SetDefaults()
        {
            hospCapTXT.Text = "" + sim.DefaultStdCapacity + "; " + sim.DefaultJIPCapacity;
            patArrTXT.Text = "" + sim.DefaultPatArrMean;
            hospStayTXT.Text = "" + sim.DefaultHospStayMean + "; " + sim.DefaultHospStayStDev;
            queueStayTXT.Text = "" + sim.DefaultQueueStayMean + "; " + sim.DefaultQueueStayStDev;
            jipStayTXT.Text = "" + sim.DefaultJIPStayMean;
            jipMoveTXT.Text = "" + sim.DefaultJIPMoveP;
            stdDeathTXT.Text = "" + sim.DefaultStdDeathP;
            jipDeathTXT.Text = "" + sim.DefaultJIPDeathP;
            deathTransferTXT.Text = "" + sim.DefaultTransferDeathP;
        }

        /// <summary>
        /// Reset list contents so it displays current data
        /// </summary>
        public void DisplayCurrentSituation()
        {
            if (listBox1.InvokeRequired)
                Invoke(new MethodInvoker(delegate () { DisplayCurrentSituation(); }));
            else
            {
                // reset list box to display all actions that has happened
                listBox1.DataSource = null;
                listBox1.DataSource = sim.actions;
                if (sim.actions.Count > 0)
                    listBox1.SetSelected(sim.actions.Count - 1, true);
            }
        }

        /// <summary>
        /// Display current statistics
        /// </summary>
        public void DisplayCurrentStats()
        {
            if (listBox2.InvokeRequired)
                Invoke(new MethodInvoker(delegate () { DisplayCurrentStats(); }));
            else
            {
                // reset list box to display all stats collected
                List<string> stats = new List<string>();

                stats.Add("# of people in hospital: " + Statistics.PeopleInHospital);
                stats.Add("# of registered patients: " + (Statistics.PatientsInSTD + Statistics.PatientsInICU));
                stats.Add("\t# in STD: " + Statistics.PatientsInSTD);
                stats.Add("\t# in ICU: " + Statistics.PatientsInICU);
                stats.Add("");
                stats.Add("# died: " + (Statistics.NumPatientsDiedWaiting + Statistics.NumPatientsDiedInStd + Statistics.NumPatientsDiedInICU));
                stats.Add("\t# waiting: " + Statistics.NumPatientsDiedWaiting);
                stats.Add("\t# in STD: " + Statistics.NumPatientsDiedInStd);
                stats.Add("\t# in ICU: " + Statistics.NumPatientsDiedInICU);
                stats.Add("");
                stats.Add("# cured: " + Statistics.NumPatientsCured);
                // stats.Add("# waiting for std: " + Statistics.NumPatientsWaitingForStd);
                // stats.Add("# waiting for icu: " + Statistics.NumPatientsWaitingForICU);
                stats.Add("Death probability: " + (int)(Statistics.PatientDeathProbability*100)/100.0);



                listBox2.DataSource = null;
                listBox2.DataSource = stats;
                if (stats.Count > 0)
                    listBox2.SetSelected(stats.Count - 1, true);
            }
        }

        /// <summary>
        /// Change button's text
        /// </summary>
        /// <param name="btn"> Button </param>
        /// <param name="txt"> Text to change it to </param>
        private void ChangeBTTxt(Button btn, string txt)
        {
            if (btn.InvokeRequired)
                Invoke(new MethodInvoker(delegate () { ChangeBTTxt(btn, txt); }));
            else
                btn.Text = txt;
        }

        /// <summary>
        /// Open error dialog
        /// </summary>
        /// <param name="title"> Title of dialog </param>
        /// <param name="unsucc"> Fail message </param>
        private void OpenResultDialog(string title, string unsucc)
        {
            if (this.InvokeRequired)
                Invoke(new MethodInvoker(delegate () { OpenResultDialog(title, unsucc); }));
            else
            {
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show(unsucc, title, buttons, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Run simulation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void runBT_Click(object sender, EventArgs e)
        {
            // hospCapTXT - zákl péče; intenzivní péče
            int[] capacity = Parser.ParseMultipleInts(hospCapTXT.Text);

            // patArrTXT - mean of patients per day
            double patientArrivalMean = Parser.ParseDouble(patArrTXT.Text);
            // hospStayTXT - mean; stDev
            double[] stay = Parser.ParseMultipleDoubles(hospStayTXT.Text);
            // queueStayTXT - mean; stDev
            double[] queueStay = Parser.ParseMultipleDoubles(queueStayTXT.Text);
            // jipStayTXT - mean
            double jipStayMean = Parser.ParseDouble(jipStayTXT.Text);

            // jipMoveTXT - probability
            double jipMoveP = Parser.ParseDouble(jipMoveTXT.Text);
            // stdDeathTXT - probability
            double stdDethP = Parser.ParseDouble(stdDeathTXT.Text);
            // jipDeathTXT - probability
            double jipDeathP = Parser.ParseDouble(jipDeathTXT.Text);
            // deathTransferTXT - probability
            double tfDeathP = Parser.ParseDouble(deathTransferTXT.Text);

            if (capacity.Length != 2 || capacity[0] == int.MinValue || capacity[1] == int.MinValue || capacity[0] < 0 || capacity[1] < 0 || 
                patientArrivalMean == double.MinValue || jipStayMean == double.MinValue || 
                jipMoveP == double.MinValue || stdDethP == double.MinValue || jipDeathP == double.MinValue || tfDeathP == double.MinValue || 
                jipMoveP < 0 || stdDethP < 0 || jipDeathP < 0 || tfDeathP < 0 ||
                stay.Length != 2 || stay[0] == double.MinValue || stay[1] == double.MinValue || stay[0] < 0 || stay[1] < 0 ||
                queueStay.Length != 2 || queueStay[0] == double.MinValue || queueStay[1] == double.MinValue || queueStay[0] < 0 || queueStay[1] < 0)
            {
                OpenResultDialog("Error", "Setting parameters was unsuccessful.");
                return;
            }

            sim.ICUCapacity = capacity[1];
            sim.StdCapacity = capacity[0];

            sim.PatArrMean = patientArrivalMean;
            sim.HospStayMean = stay[0];
            sim.HospStayStDev = stay[1];
            sim.ICUStayMean = jipStayMean;

            sim.ICUMoveP = jipMoveP;
            sim.ICUDeathP = jipDeathP;
            sim.StdDeathP = stdDethP;
            sim.transferDeathP = tfDeathP;

            sim.QueueStayMean = queueStay[0];
            sim.QueueStayStDev = queueStay[1];

            paused = false;
            ChangeBTTxt(pauseBT, "Pause");

            simRunning = true;
            simThread = new Thread(() =>
            {
                sim.SimRoutine();
            });
            simThread.Start();
        }

        /// <summary>
        /// Stop simulation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void stopBT_Click(object sender, EventArgs e)
        {
            sim.Stop();
            DisplayCurrentSituation();
            ChangeBTTxt(pauseBT, "Pause");
            paused = true;
            simRunning = false;
        }

        /// <summary>
        /// Pause simulation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pauseBT_Click(object sender, EventArgs e)
        {
            paused = !paused;

            sim.Pause();
            if (paused)
                ChangeBTTxt(pauseBT, "Resume");
            else
                ChangeBTTxt(pauseBT, "Pause");
        }

        /// <summary>
        /// Closing form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosing(Object sender, FormClosingEventArgs e)
        {
            sim.Stop();
            if (simThread != null && simThread.IsAlive)
                simThread.Abort();
        }

        /// <summary>
        /// Show statistics
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void showStatsBT_Click(object sender, EventArgs e)
        {
            if (simRunning)
            {
                StatsForm stats = new StatsForm();
                stats.Show();
            }
        }
    }
}
